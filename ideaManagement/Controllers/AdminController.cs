﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ideaManagement.Models;
using System.Web.Script.Serialization;

namespace ideaManagement.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Staff()
        {
            return View();
        }

        public ActionResult Department()
        {
            return View();
        }

        public JsonResult LoginProcess(string password)
        {
            if(password == "123")
            {
                return Json("{\"Status\":\"Admin\"}", JsonRequestBehavior.AllowGet);
            }
            return Json("{\"Status\":\"Error\"}", JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStaffandDepartment(string password)
        {
            StaffDb staffDb = new StaffDb();
            DepartmentDb departmentDb = new DepartmentDb();            

            List<Staff> staffList = staffDb.getAll();
            List<Department> departmentList = departmentDb.getAll();

            JavaScriptSerializer jss = new JavaScriptSerializer();

            string output1 = jss.Serialize(staffList);
            string output2 = jss.Serialize(departmentList);

            string output = '[' + output1 + "," + output2 + ']';

            return Json(output, JsonRequestBehavior.AllowGet);            
        }

        public JsonResult CreateStaff(Staff staffData)
        {
            StaffDb staffDb = new StaffDb();
            staffDb.CreateNew(staffData);
            return Json("{\"Status\":\"Ok\"}", JsonRequestBehavior.AllowGet);
        }
    }
}