﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ideaManagement.Models
{
    public class Department
    {
        public int departmentId { set; get; }
        public string departmentName { set; get; }
        public int stationeryManagerId { set; get; }
    }
}