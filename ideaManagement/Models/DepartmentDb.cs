﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace ideaManagement.Models
{
    public class DepartmentDb
    {
        string connectionStr = "Data Source =DESKTOP-FAQIU16\\SQLEXPRESS11; Initial Catalog = ideaManagement; Integrated Security = True;";

        public List<Department> getAll()
        {
            string strCommand = @"SELECT * FROM department";
            List<Department> list = new List<Department>();
            using (SqlConnection con = new SqlConnection(connectionStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(strCommand, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Department
                            {
                                departmentId = Convert.ToInt32(reader["departmentId"]),
                                departmentName = reader["departmentName"].ToString(),
                                stationeryManagerId = Convert.ToInt32(reader["stationeryManagerId"])
                            });
                        }
                        return list;
                    }
                }
            }
        }
    }
}