﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ideaManagement.Models
{
    public class Staff
    {
        public int staffId { set; get; }
        public string staffName { set; get; }
        public int departmentId { set; get; }
    }
}