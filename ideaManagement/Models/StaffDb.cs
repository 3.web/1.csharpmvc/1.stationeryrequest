﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace ideaManagement.Models
{
    public class StaffDb
    {
        string connectionStr = "Data Source =DESKTOP-FAQIU16\\SQLEXPRESS11; Initial Catalog = ideaManagement; Integrated Security = True;";

        public List<Staff> getAll()
        {
            string strCommand = @"SELECT * FROM staff";
            List<Staff> list = new List<Staff>();
            using (SqlConnection con = new SqlConnection(connectionStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(strCommand, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Staff
                            {
                                staffId = Convert.ToInt32(reader["staffId"]),
                                staffName = reader["staffName"].ToString(),
                                departmentId = Convert.ToInt32(reader["departmentId"])
                            });
                        }
                        return list;
                    }
                }
            }
        }

        public void CreateNew(Staff staffData)
        {
            string strCommand = @"INSERT INTO staff(staffId, staffName, departmentId) VALUES(@staffId, @staffName, @departmentId)";
            using (SqlConnection con = new SqlConnection(connectionStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(strCommand, con))
                {
                    cmd.Parameters.AddWithValue("@staffId", staffData.staffId);
                    cmd.Parameters.AddWithValue("@staffName", staffData.staffName);
                    cmd.Parameters.AddWithValue("@departmentId", staffData.departmentId);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}