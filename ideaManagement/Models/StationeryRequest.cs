﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ideaManagement.Models
{
    public class StationeryRequest
    {
        public int id { set; get; }
        public string name { set; get; }
        public int quantity { set; get; }
        public string link { set; get; }
        public string staffId { set; get; }
        public string note { set; get; }
        public int status { set; get; }
        public int departmentId { set; get; }
    }
}