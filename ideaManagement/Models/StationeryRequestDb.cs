﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace ideaManagement.Models
{
    public class StationeryRequestDb
    {
        string connectionStr = "Data Source =DESKTOP-FAQIU16\\SQLEXPRESS11; Initial Catalog = ideaManagement; Integrated Security = True;";

        public List<StationeryRequest> getAll()
        {
            string strCommand = @"SELECT * FROM stationeryRequest";
            List<StationeryRequest> list = new List<StationeryRequest>();
            using (SqlConnection con = new SqlConnection(connectionStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(strCommand, con))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new StationeryRequest
                            {
                                id = Convert.ToInt32(reader["id"]),
                                name = reader["staffId"].ToString()
                            });
                        }
                        return list;
                    }
                }
            }
        }

        public void AddStationeryRequest()
        {
            string strCommand = @"INSERT INTO stationeryRequest(name, quantity, link, staffId, note) VALUES(@name, @quantity, @link, @staffId, @note)";
            Guid firmwareUid = Guid.NewGuid();
            using (SqlConnection con = new SqlConnection(connectionStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(strCommand, con))
                {
                    cmd.Parameters.AddWithValue("@name", "nameTest");
                    cmd.Parameters.AddWithValue("@quantity", 1);
                    cmd.Parameters.AddWithValue("@link", 1);
                    cmd.Parameters.AddWithValue("@staffId", "211");
                    cmd.Parameters.AddWithValue("@note", "no");

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}