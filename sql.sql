USE [ideaManagement]
GO
/****** Object:  Table [dbo].[department]    Script Date: 02/08/2020 8:48:13 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[department](
	[departmentId] [bigint] NOT NULL,
	[departmentName] [nvarchar](50) NULL,
	[stationeryManagerId] [bigint] NULL,
 CONSTRAINT [PK_department] PRIMARY KEY CLUSTERED 
(
	[departmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[staff]    Script Date: 02/08/2020 8:48:15 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[staff](
	[staffId] [bigint] NOT NULL,
	[staffName] [nvarchar](50) NULL,
	[departmentId] [bigint] NULL,
 CONSTRAINT [PK_staff] PRIMARY KEY CLUSTERED 
(
	[staffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[stationeryRequest]    Script Date: 02/08/2020 8:48:15 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stationeryRequest](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[quantity] [int] NULL,
	[link] [nvarchar](max) NULL,
	[staffId] [bigint] NULL,
	[note] [nvarchar](max) NULL,
	[status] [int] NULL,
	[departmentId] [bigint] NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_stationeryRequest] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[stationeryRequest] ADD  CONSTRAINT [DF_stationeryRequest_status]  DEFAULT ((0)) FOR [status]
GO
